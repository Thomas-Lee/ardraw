#include <Arduino.h>
#include <AccelStepper.h>

// Stepper control Thomas Lee 2013

// Create stepper
AccelStepper stepper_A(AccelStepper::DRIVER, 9, 8);
AccelStepper stepper_B(AccelStepper::DRIVER, 7, 6);

void serialRX();
void serialTX();

//RX
#define bufferSizeRX 20

char bufferRX[bufferSizeRX];
char bufferCharRX;
int bufferIndexRX;

//Strtok
#define commandSizeRX 5 // header,positionA,speedA,positionB,speedB#

const char* delimiterRX = ",";
const char* commandRX[commandSizeRX];
char* strPointerRX;

//TX
#define commandSizeTX 40
#define delayTX 20

char commandTXChar;
char commandTX[commandSizeTX];
int commandStrSizeTX;

signed long distanceA, distanceB;

unsigned long currentMillis, previousMillis;

void setup(){

  Serial.begin(9600);

  commandTXChar = 'R';

  pinMode(13,OUTPUT);

  stepper_A.setMaxSpeed(500);
  //stepper_A.setAcceleration(1000);

  stepper_B.setMaxSpeed(500);
  //stepper_B.setAcceleration(1000);

}


void loop(){

	currentMillis = millis();

	  serialRX();
	  serialTX();

	  stepper_A.runSpeedToPosition();
	  stepper_B.runSpeedToPosition();

	  distanceA = stepper_A.distanceToGo();
	  distanceB = stepper_B.distanceToGo();

	  if(distanceA == 0 && distanceB == 0){
	    commandTXChar = 'R';
	  } else {
	    commandTXChar = 'M';
	  }


}

void serialRX() {
while(Serial.available() > 0 ){

    bufferCharRX = Serial.read();
    delay(1);

    if(bufferCharRX != '#'){
      bufferRX[bufferIndexRX] = bufferCharRX;
      bufferIndexRX++;
    }
    else {

      //Include terminating NULL in buffer end - strtok looks for it to stop
      bufferRX[bufferIndexRX] = '\0';

      //StrTok delimit buffer
      strPointerRX = strtok(bufferRX, delimiterRX);
      for(int i=0; i < commandSizeRX; i++){
        commandRX[i] = strPointerRX;
        strPointerRX = strtok(NULL, delimiterRX);
      }

      if(*commandRX[0]=='D'){

        //Set motor position
        stepper_A.moveTo(atoi(commandRX[1]));
        stepper_A.setSpeed(atoi(commandRX[2]));
        stepper_B.moveTo(atoi(commandRX[3]));
        stepper_B.setSpeed(atoi(commandRX[4]));
        //commandTXChar = 'D';
      }
      else if(*commandRX[0]=='R'){
        stepper_A.setCurrentPosition(atoi(commandRX[1]));
        stepper_B.setCurrentPosition(atoi(commandRX[2]));
        digitalWrite(13,HIGH);
        delay(100);
        digitalWrite(13, LOW);

      }
      bufferIndexRX=0;
    }
}
}

void serialTX(){
    if(currentMillis - previousMillis > delayTX){
    commandStrSizeTX = sprintf (commandTX, "%c,%ld,%ld%c", commandTXChar, distanceA, distanceB, '#');
    Serial.write((unsigned char*)commandTX, commandStrSizeTX);
    previousMillis = currentMillis;
    }

}
